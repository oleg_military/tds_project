#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDS_StateEffect.generated.h"

UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDS_StateEffect : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	bool bIsStakable = false;

	AActor* MyActor = nullptr;

	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();
	
};

UCLASS()
class TDS_API UTDS_StateEffect_ExecuteOnce : public UTDS_StateEffect
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SettingsExecuteOnce")
	float Power = 20.f;

	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

};

UCLASS()
class TDS_API UTDS_StateEffect_ExecuteTimer : public UTDS_StateEffect
{
	GENERATED_BODY()

public:
	UParticleSystemComponent* ParticleComponent = nullptr;

	FTimerHandle ExicuteTimerHandle;
	FTimerHandle EffectTimerHandle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SettingsExecuteTimer")
	UParticleSystem* ParticleEffect = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SettingsExecuteTimer")
	float Power = 20.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SettingsExecuteTimer")
	float Timer = 5.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SettingsExecuteTimer")
	float RateTime = 1.f;

	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();

};
