#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChangeSignature, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeadSignature);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTDSHealthComponent();

	UPROPERTY(BlueprintAssignable, EditDefaultsOnly, BlueprintReadWrite, Category = "Delegates")
	FOnHealthChangeSignature OnHealthChange;

	UPROPERTY(BlueprintAssignable, EditDefaultsOnly, BlueprintReadWrite, Category = "Delegates")
	FOnDeadSignature OnDead;



protected:
	virtual void BeginPlay() override;

	float Health = 100.0f;

public:	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
	float CoefDamage = 1.0f;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetCurrentHealth(float NewHealth);

	UFUNCTION(BlueprintCallable, Category = "Health")
	virtual void ChangeHealthValue(float Value);

};
