#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSTypes.h"
#include "InventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeaponSignature, FName, WeaponID_Name, FAdditionalWeaponData, WeaponAdditionalInfo, int32, NewWeaponIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChangeSignature, EWeaponType, AmmoType, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChangeSignature, int32, IndexSlot, FAdditionalWeaponData, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAviableSignature, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmptySignature, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlotsSignature, int32, IndexSlotChange, FWeaponSlotData, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRoundSignature, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRoundSignature, int32, IndexSlotWeapon);

class ATDSWeaponPickup;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UInventoryComponent();

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnSwitchWeaponSignature OnSwitchWeapon;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnAmmoChangeSignature OnAmmoChange;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAdditionalInfoChangeSignature OnWeaponAdditionalInfo;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAmmoAviableSignature OnWeaponAmmoAviable;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAmmoEmptySignature OnWeaponAmmoEmpty;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnUpdateWeaponSlotsSignature OnUpdateWeaponSlots;

	//Event current weapon not have additional_Rounds 
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponNotHaveRoundSignature OnWeaponNotHaveRound;
	
	//Event current weapon have additional_Rounds 
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponHaveRoundSignature OnWeaponHaveRound;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	TArray<FWeaponSlotData> WeaponSlots;

	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	TArray<FAmmoSlotData> AmmoSlots;

	int32 MaxSlotWeapon = 0;

public:
	bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponData OldInfo, bool bIsForward);
	bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponData PreviosWeaponInfo);

	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponData NewInfo);

	FAdditionalWeaponData GetAdditionalInfoWeapon(int32 IndexWeapon);
	int32 GetWeaponIndexSlotByName(FName WeaponID_Name);
	FName GetWeaponNameBySlotIndex(int32 IndexSlot) const;

	bool GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType);
	bool GetWeaponTypeByNameWeapon(FName WeaponID_Name, EWeaponType& WeaponType);

	UFUNCTION(BlueprintCallable)
	void AmmoSlotChangeValue(EWeaponType WeaponType, int32 CoutChangeAmmo);

	bool CheckAmmoForWeapon(EWeaponType WeaponType, int32& AviableAmmoForWeapon);

	// Interface PickUp Actors
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeAmmo(EWeaponType AmmoType);

	// Interface PickUp Actors
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeWeapon(int32& FreeSlot);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool SwitchWeaponToInventory(FWeaponSlotData NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItemData& DropItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItemData& DropItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool TryGetWeaponToInventory(FWeaponSlotData NewWeapon);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	void DropWeaponByIndex(int32 ByIndex, FDropItemData& DropItemData);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<FWeaponSlotData> GetWeaponSlotsData() const;

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<FAmmoSlotData> GetAmmoSlotsData() const;

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void InitInventory_OnServer(const TArray<FWeaponSlotData>& NewWeaponSlotsData, const TArray<FAmmoSlotData>& NewAmmoSlotsData);

};