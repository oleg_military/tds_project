#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDSTypes.h"
#include "TDSGameInstance.generated.h"

class UDataTable;

UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponData& OutInfo);

	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByWeaponName(FName NameItem, FDropItemData& OutInfo);

	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByName(FName NameItem, FDropItemData& OutInfo);

protected:
	// Tables
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Tables | Weapons")
	UDataTable* WeaponDataTable = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Tables | DropWeapon")
	UDataTable* DropItemDataTable = nullptr;
	
};
