#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "TDSTypes.generated.h"

class AWeaponBase;
class AProjectile;
class ATDSWeaponPickup;
class UTDS_StateEffect;
class ADropShell;
class UNiagaraSystem;
class USoundCue;

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	AIMING_STATE UMETA(DisplayName = "Aiming state"),
	AIMING_WALK_STATE UMETA(DisplayName = "Aiming walk state"),
	WALK_STATE UMETA(DisplayName = "Walk state"),
	RUN_STATE UMETA(DisplayName = "Run state"),
	SPRINT_STATE UMETA(DisplayName = "Sprint state")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	NONE_TYPE UMETA(DisplayName = "None"),
	SHOTGUN_TYPE UMETA(DisplayName = "ShotGun"),
	RIFLE_TYPE UMETA(DisplayName = "Rifle"),
	SNIPER_RIFLE_TYPE UMETA(DisplayName = "SniperRifle"),
	GRENADE_LAUNCHER_TYPE UMETA(DisplayName = "GrenadeLauncher"),
	ROCKET_LAUNCHER_TYPE UMETA(DisplayName = "RocketLauncher")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float AimingSpeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float AimingWalkSpeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float RunSpeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float SprintSpeed;

	FCharacterSpeed()
		: AimingSpeed(250.f)
		, AimingWalkSpeed(150.f)
		, WalkSpeed(200.f)
		, RunSpeed(450.f)
		, SprintSpeed(600.f)
	{
	}
};

USTRUCT(BlueprintType)
struct FDecalData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Effect")
	UMaterialInterface* Material;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Effect")
	FVector Size;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Effect")
	float LifeTime;

	FDecalData()
		: Material(nullptr)
		, Size(FVector(20.f))
		, LifeTime(5.f)
	{
	}
};

USTRUCT(BlueprintType)
struct FImpactData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Effects")
	FDecalData DecalData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Effects")
	UParticleSystem* HitEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Effects")
	USoundCue* HitSound;

	FImpactData()
		: DecalData(FDecalData())
		, HitEffect(nullptr)
		, HitSound(nullptr)
	{
	}
};

USTRUCT(BlueprintType)
struct FExplosionData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Explosion")
	UParticleSystem* ExplosionFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Explosion")
	USoundCue* ExplosionSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Explosion")
	float ProjectileMinRadiusDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Explosion")
	float ProjectileMaxRadiusDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Explosion")
	float ExplodeMaxDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Explosion")
	float ExplodeFalloffCoef;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Explosion")
	bool ExplodeOnImpact;

	FExplosionData()
		: ExplosionFX(nullptr)
		, ExplosionSound(nullptr)
		, ProjectileMinRadiusDamage(200.f)
		, ProjectileMaxRadiusDamage(200.f)
		, ExplodeMaxDamage(40.f)
		, ExplodeFalloffCoef(1.f)
		, ExplodeOnImpact(false)
	{
	}
};

USTRUCT(BlueprintType)
struct FTraceData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	float TraceDamage;	
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	float TraceDistacne;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	UNiagaraSystem* TraceEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	FString TraceTargetName;

	/** Defaults value */
	FTraceData()
		: TraceDamage(20.f)
		, TraceDistacne(2000.f)
		, TraceEffect(nullptr)
		, TraceTargetName("TraceTarget")
	{		
	}
};

USTRUCT(BlueprintType)
struct FProjectileData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSetting")
	TSubclassOf<AProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileLifeTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileInitSpeed;	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Effect")
	TSubclassOf<UTDS_StateEffect> Effect;

	//Explosion settings
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Explosion")
	FExplosionData ExplosionData;

	FProjectileData()
		: ProjectileClass(nullptr)
		, ProjectileDamage(20.f)
		, ProjectileLifeTime(10.f)
		, ProjectileInitSpeed(2000.f)
		, Effect(nullptr)
		, ExplosionData(FExplosionData())
	{
	}
};

USTRUCT(BlueprintType)
struct FFireData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire settings")
	bool bUseProjectile;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire settings", meta = (DisplayName = "TraceSettings", EditCondition = "!bUseProjectile"))
	FTraceData TraceData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire settings", meta = (DisplayName = "ProjectileSettings", EditCondition = "bUseProjectile"))
	FProjectileData ProjectileData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire settings")
	TMap<TEnumAsByte<EPhysicalSurface>, FImpactData> ImpactMapData;

	FFireData()
		: bUseProjectile(true)
		, TraceData(FTraceData())
		, ProjectileData(FProjectileData())
		, ImpactMapData()
	{
	}

};

USTRUCT(BlueprintType)
struct FWeaponDispersionData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float AimingDispersionMin;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float AimingDispersionMax;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float AimingDispersionRecoil;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float AimingDispersionReduction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float AimingWalkDispersionMin;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float AimingWalkDispersionMax;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float AimingWalkDispersionRecoil;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float AimingWalkDispersionReduction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WalkDispersion")
	float WalkDispersionMin;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float WalkDispersionMax;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float WalkDispersionRecoil;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float WalkDispersionReduction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float RunDispersionMin;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float RunDispersionMax;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float RunDispersionRecoil;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	float RunDispersionReduction;

	/** Defaults value */
	FWeaponDispersionData()
		//Aiming state
		: AimingDispersionMin(.3f)
		, AimingDispersionMax(2.f)
		, AimingDispersionRecoil(1.f)
		, AimingDispersionReduction(.3f)
		//Aiming walk state
		, AimingWalkDispersionMin(.1f)
		, AimingWalkDispersionMax(1.f)
		, AimingWalkDispersionRecoil(1.f)
		, AimingWalkDispersionReduction(.4f)
		//Walk state
		, WalkDispersionMin(1.f)
		, WalkDispersionMax(5.f)
		, WalkDispersionRecoil(1.f)
		, WalkDispersionReduction(.2f)
		//Run state
		, RunDispersionMin(4.f)
		, RunDispersionMax(10.f)
		, RunDispersionRecoil(1.f)
		, RunDispersionReduction(.1f)
	{	
	}
};

USTRUCT(BlueprintType)
struct FCharacterAnimData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage* CharacterFireHip;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage* CharacterFireIronsights;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage* CharacterReloadHip;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage* CharacterReloadIronsights;

	FCharacterAnimData()
		: CharacterFireHip(nullptr)
		, CharacterFireIronsights(nullptr)
		, CharacterReloadHip(nullptr)
		, CharacterReloadIronsights(nullptr)
	{
	}
};

USTRUCT(BlueprintType)
struct FWeaponAnimData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage* WeaponFire;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage* WeaponReloadHip;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage* WeaponReloadIronsights;

	FWeaponAnimData()
		: WeaponFire(nullptr)
		, WeaponReloadHip(nullptr)
		, WeaponReloadIronsights(nullptr)
	{
	}
};

USTRUCT(BlueprintType)
struct FWeaponEffectData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Effect")
	UParticleSystem* FireEffect;

	FWeaponEffectData()
		: FireEffect(nullptr)
	{		
	}
};

USTRUCT(BlueprintType)
struct FDropMeshData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mesh")
	TSubclassOf<ADropShell> DropItemClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	float DropDelay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	float LifeTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	float DropImpulse;

	FDropMeshData()
		: DropItemClass(nullptr)
		, DropDelay(-1.f)
		, LifeTime(3.f)
		, DropImpulse(150.f)
	{
	}
};

USTRUCT(BlueprintType)
struct FWeaponData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Class")
	TSubclassOf<AWeaponBase> WeaponClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base settings", meta = (ClampMin = "0"))
	float RateOfFire;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base settings", meta = (ClampMin = "0"))
	float ReloadTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base settings", meta = (ClampMin = "0"))
	int32 ClipSize;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base settings", meta = (ClampMin = "1"))
	int32 NumberBulletsByShot;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FireSettings", meta = (DisplayName = "FireSettings"))
	FFireData FireData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire settings")
	FWeaponEffectData WeaponEffects;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dispersion")
	FWeaponDispersionData DispersionWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sounds")
	USoundCue* SoundFireWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sounds")
	USoundCue* SoundReloadWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	FCharacterAnimData CharacterAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	FWeaponAnimData WeaponAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
	FDropMeshData DropSleeve;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
	FDropMeshData DropClip;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory")
	UTexture2D* WeaponIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory")
	EWeaponType WeaponType;

	FWeaponData()
		: WeaponClass(nullptr)
		, RateOfFire(.5f)
		, ReloadTime(2.f)
		, ClipSize(1.f)
		, NumberBulletsByShot(1.f)
		, FireData(FFireData())
		, WeaponEffects(FWeaponEffectData())
		, DispersionWeapon(FWeaponDispersionData())
		, SoundFireWeapon(nullptr)
		, SoundReloadWeapon(nullptr)
		, CharacterAnimation(FCharacterAnimData())
		, WeaponAnimation(FWeaponAnimData())
		, DropSleeve(FDropMeshData())
		, DropClip(FDropMeshData())
		, WeaponIcon(nullptr)
		, WeaponType(EWeaponType::NONE_TYPE)
	{
	}
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponStats")
	int32 Round;

	/** Defaults value */
	FAdditionalWeaponData()
		: Round(0)
	{
	}
};

USTRUCT(BlueprintType)
struct FWeaponSlotData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSlot")
	FName WeaponID_Name;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSlot")
	FAdditionalWeaponData AdditionalInfo;

	/** Defaults value */
	FWeaponSlotData() 
		: WeaponID_Name(NAME_None)
		, AdditionalInfo(FAdditionalWeaponData())
	{
	}
};

USTRUCT(BlueprintType)
struct FAmmoSlotData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AmmoSlot")
	EWeaponType WeaponType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AmmoSlot", meta = (ClampMin = "0"))
	int32 Bullets;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AmmoSlot", meta = (ClampMin = "0"))
	int32 MaxBullets;

	/** Defaults value */
	FAmmoSlotData()
		: WeaponType(EWeaponType::NONE_TYPE)
		, Bullets(0)
		, MaxBullets(0)
	{
	}
};

USTRUCT(BlueprintType)
struct FDropItemData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DropItem")
	UStaticMesh* WeaponStaticMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DropItem")
	USkeletalMesh* WeaponSkeletalMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DropItem")
	UParticleSystem* ParticleItem;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DropItem")
	FTransform Offset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DropItem")
	FWeaponSlotData WeaponInfo;

	/** Defaults value */
	FDropItemData()
		: WeaponStaticMesh(nullptr)
		, WeaponSkeletalMesh(nullptr)
		, ParticleItem(nullptr)
		, Offset(FTransform())
		, WeaponInfo(FWeaponSlotData())
	{
	}
};

UCLASS()
class TDS_API UTDSTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTDS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType);

};