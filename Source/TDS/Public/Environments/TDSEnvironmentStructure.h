#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/TDS_IGameActor.h"
#include "TDSEnvironmentStructure.generated.h"

class UTDS_StateEffect;

UCLASS()
class TDS_API ATDSEnvironmentStructure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	ATDSEnvironmentStructure();

	//Effects
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UTDS_StateEffect*> Effects;

protected:
	virtual void BeginPlay() override;

public:	
	/* Interface start*/
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTDS_StateEffect*> GetAllCurrentEffects() const override;

	void RemoveEffect(UTDS_StateEffect* RemoveEffect) override;
	void AddEffect(UTDS_StateEffect* NewEffect) override;
	/* Interface end*/

};
