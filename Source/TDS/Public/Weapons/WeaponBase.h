#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TDSTypes.h"
#include "Weapons/Projectiles/Projectile.h"
#include "WeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, FireAnim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, ReloadAnim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoTake);

UCLASS()
class TDS_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

	/** Delegates */
	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	/** Structures */
	UPROPERTY(VisibleAnywhere)
	FWeaponData WeaponSettings;
	
	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponData AdditionalWeaponInfo;

	/** For Debug!!! */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTime = 0.0f;

	UPROPERTY(Replicated)
	FVector ShootEndLocation = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;

	/** Flags */
	bool bBlockFire = false;

	/** Dispersion */
	bool bShouldReduceDispersion = false;

	FName WeaponID_Name = NAME_None;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class UArrowComponent* SleeveDropLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class UArrowComponent* ClipDropLocation = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FireLogic", meta = (AllowPrivateAccess = "true"))
	bool bWeaponFiring = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ReloadLogic", meta = (AllowPrivateAccess = "true"))
	bool bWeaponReloading = false;

	/** For Debug */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug", meta = (AllowPrivateAccess = "true"))
	bool bFireAtStartGame = false;

	/** Dispersion */
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	/* Timers flags */
	float FireTimer = 0.0f;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ReloadLogic", meta = (AllowPrivateAccess = "true"))
	float ReloadTimer = 0.0f;
	
	bool bAiming = false;

	bool bDropSleeve = false;
	bool bDropClip = false;	
	
	float DropSleeveTimer = -1.0f;
	float DropClipTimer = -1.0f;

public:	
	virtual void Tick(float DeltaTime) override;
	
	void FireTick(float DeltaTime);	
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void DropMeshTick(float DeltaTime);

	/** Weapons */
	void WeaponInit();
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);	

	void Fire();

	bool CheckCanWeaponReload();
	int32 GetAviableAmmoForReload();

	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);
	
	void ChangeDispersionByShot();

	void InitReload();
	void CancelReload();

	/** Get & Set functions */
	FVector GetFireEndLocation() const;

	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	float GetCurrentDispersion() const;

	int8 GetNumberProjectileByShot() const;

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound() const;

	bool CheckWeaponCanFire() const;

	bool GetWeaponReloading() const;

	bool CanWeaponReload();

	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool NewShootReduceDispersion);

	UFUNCTION(NetMulticast, Unreliable)
	void ShellDropFire_Multicast();

	UFUNCTION(NetMulticast, Unreliable)
	void AnimWeaponStart_Multicast(UAnimMontage* Anim);

	UFUNCTION(NetMulticast, Unreliable)
	void WeaponFireFX_Multicast(UParticleSystem* FireFX, USoundCue* Sound);

private:
	void FinishReload();
	void PlayReloadFX();

	void InitDropMesh(FDropMeshData DropMeshInfo, UArrowComponent* DropTransform);
};
