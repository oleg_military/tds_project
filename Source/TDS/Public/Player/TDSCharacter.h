#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDSTypes.h"
#include "Interfaces/TDS_IGameActor.h"
#include "TDSCharacter.generated.h"

class AWeaponBase;
class UTDS_StateEffect;

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TDSCamera; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/** Inventory component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	class UInventoryComponent* InventoryComponent;

	/** Health component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	class UTDSCharacterHealthComponent* HealthComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	/** Cursor */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	class UDecalComponent* CurrentCursor = nullptr;;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")	
	UMaterialInterface* CursorMaterial = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")	
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Effects
	TArray<UTDS_StateEffect*> Effects;

protected:
	UPROPERTY(Replicated)
	EMovementState MovementState = EMovementState::RUN_STATE;

	UPROPERTY(Replicated)
	class AWeaponBase* CurrentWeapon = nullptr;

	int32 CurrentWeaponIndex = 0;

	bool bIsAlive = true;

	virtual void BeginPlay() override;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TDSCamera;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;	

	FTimerHandle RagdollTimer;

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	/** Set movement mode */
	bool bAiming = false;
	bool bWalking = false;
	bool bSprinting = false;

	void ChangingAimingScale(EMovementState CurrentState);

public:
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	void MovementTick(float DeltaTime);

	/** Setup InputComponent */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	/** Movement Events */
	void InputAxisX(float Value);
	void InputAxisY(float Value);

	/** Fire Events */
	void InputAttackPressed();
	void InputAttackReleased();
	
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	/** Switch walking mode */
	void WalkingEnable();
	void WalkingDisable();

	/** Switch aiming mode */
	void AimingEnable();
	void AimingDisable();

	/** Switch sprinting mode */
	void SprintingEnable();
	void SprintingDisable();

	void TryAbilityAction();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}

	void DropCurrentWeapon();

	/** Update functions */
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName WeaponNameID, FAdditionalWeaponData WeaponAdditionalInfo, int32 NewWeaponIndex);
	
	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
	
	UFUNCTION(BlueprintImplementableEvent)
	void WeaponReloadStart_BP(class UAnimMontage* Anim);
	
	UFUNCTION(BlueprintImplementableEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);
	
	UFUNCTION(BlueprintImplementableEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);

	/** Returns functions */
	UFUNCTION(BlueprintCallable)
	class UDecalComponent* GetCursorToWorld();
	
	UFUNCTION(BlueprintCallable)
	class AWeaponBase* GetCurrentWeapon() const;
	
	UFUNCTION()
	APlayerController* GetPlayerController() const;

	/** Returns Movements mode */
	UFUNCTION(BlueprintCallable)
	bool GetAimingMode() const;
	
	UFUNCTION(BlueprintCallable)
	bool GetWalkingMode() const;
	
	UFUNCTION(BlueprintCallable)
	bool GetSprintingMode() const;

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateCameraDistance(EMovementState CurrentState);

	void UpdateAmmoCout();	

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentWeaponIndex();

	/* Interface start*/
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTDS_StateEffect*> GetAllCurrentEffects() const override;

	void RemoveEffect(UTDS_StateEffect* RemoveEffect) override;
	void AddEffect(UTDS_StateEffect* NewEffect) override;
	/* Interface end*/

	UFUNCTION(BlueprintNativeEvent)
	void CharacterDead_BP();

	UFUNCTION(BlueprintCallable)
	bool GetIsAlive() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	EMovementState GetMovementState() const;

	UFUNCTION(Server, Unreliable)
	void SetActorRotationYaw_OnServer(float Yaw);

	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewState);

	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewState);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
	TArray<UAnimMontage*> DeathsAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
	TSubclassOf<UTDS_StateEffect> AbilityEffect;	

	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
	
	UFUNCTION()
	void WeaponReloadStart(class UAnimMontage* Anim);
	
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);

private:
	void DisplacementUpdate(const FHitResult& HitResult);

	void TrySwicthNextWeapon();
	void TrySwitchPreviousWeapon();
	void SwitchWeapon(bool bIsForward);

	UFUNCTION()
	void CharacterDead();

	void EnableRagdoll();

	bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);

};