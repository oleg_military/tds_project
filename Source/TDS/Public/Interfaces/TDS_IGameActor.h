#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDSTypes.h"
#include "TDS_IGameActor.generated.h"

class UTDS_StateEffect;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

class TDS_API ITDS_IGameActor
{
	GENERATED_BODY()

public:
	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTDS_StateEffect*> GetAllCurrentEffects() const;
	virtual void RemoveEffect(UTDS_StateEffect* RemoveEffect);
	virtual void AddEffect(UTDS_StateEffect* NewEffect);

	//Interface for inventory
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItemData DropItemData);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(FDropItemData DropItemData);

};
