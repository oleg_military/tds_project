using UnrealBuildTool;

public class TDS : ModuleRules
{
	public TDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] 
		{
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore",
			"Niagara",
			"HeadMountedDisplay", 
			"NavigationSystem", 
			"AIModule",
			"PhysicsCore",
			"SlateCore",
			"Slate"
		});

		PrivateDependencyModuleNames.AddRange(new string[] 
		{ });

		PublicIncludePaths.AddRange(new string[]
        {
			"TDS/Public/Components",
			"TDS/Public/Environments",
			"TDS/Public/Game",
			"TDS/Public/Interfaces",
			"TDS/Public/Player",
			"TDS/Public/StateEffects",
			"TDS/Public/Weapons",
			"TDS/Public/Weapons/Items",
			"TDS/Public/Weapons/Projectiles"
		});
    }
}
