#include "Player/TDSCharacter.h"

// UE4 class include
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/ActorComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Net/UnrealNetwork.h"

//Game class include
#include "TDS.h"
#include "Game/TDSGameInstance.h"
#include "Weapons/WeaponBase.h"
#include "StateEffects/TDS_StateEffect.h"
#include "Weapons/Projectiles/Projectile.h"
#include "Components/InventoryComponent.h"
#include "Components/TDSCharacterHealthComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDSCharacter, All, All);

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

										  // Create a camera...
	TDSCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TDSCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TDSCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent)
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);

	HealthComponent = CreateDefaultSubobject<UTDSCharacterHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
		HealthComponent->OnDead.AddDynamic(this, &ATDSCharacter::CharacterDead);

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	bReplicates = true;
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector::ZeroVector);
		}
	}

	ChangeMovementState();
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		const auto MyPC = GetPlayerController();
		if (MyPC && MyPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			MyPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	if (!bIsAlive) return;

	const auto MyPC = GetPlayerController();
	if (!MyPC) return;

	if (GetController()->IsLocalPlayerController())
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		FString SEnum = UEnum::GetValueAsString(MovementState);
		UE_LOG(LogTDS_Net, Display, TEXT("Movement State: %s"), *SEnum);
	
		if (MovementState == EMovementState::SPRINT_STATE)
		{
			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			FRotator myRotator = myRotationVector.ToOrientationRotator();		
			
			SetActorRotation(FQuat(myRotator));
			SetActorRotationYaw_OnServer(myRotator.Yaw);
		}
		else
		{	
			FHitResult HitResult;
			MyPC->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);

			float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
			SetActorRotationYaw_OnServer(FindRotaterResultYaw);

			DisplacementUpdate(HitResult);
		}
	}
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	PlayerInputComponent->BindAction("WalkingEvent", IE_Pressed, this, &ATDSCharacter::WalkingEnable);
	PlayerInputComponent->BindAction("WalkingEvent", IE_Released, this, &ATDSCharacter::WalkingDisable);
	
	PlayerInputComponent->BindAction("AiminigEvent", IE_Pressed, this, &ATDSCharacter::AimingEnable);
	PlayerInputComponent->BindAction("AiminigEvent", IE_Released, this, &ATDSCharacter::AimingDisable);

	PlayerInputComponent->BindAction("SprintingEvent", IE_Pressed, this, &ATDSCharacter::SprintingEnable);
	PlayerInputComponent->BindAction("SprintingEvent", IE_Released, this, &ATDSCharacter::SprintingDisable);

	PlayerInputComponent->BindAction("SwitchNextWeapon", IE_Pressed, this, &ATDSCharacter::TrySwicthNextWeapon);
	PlayerInputComponent->BindAction("SwitchPreviousWeapon", IE_Released, this, &ATDSCharacter::TrySwitchPreviousWeapon);

	PlayerInputComponent->BindAction("FireEvent", IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction("FireEvent", IE_Released, this, &ATDSCharacter::InputAttackReleased);

	PlayerInputComponent->BindAction("ReloadEvent", IE_Pressed, this, &ATDSCharacter::TryReloadWeapon);

	PlayerInputComponent->BindAction("AbilityAction", IE_Pressed, this, &ATDSCharacter::TryAbilityAction);

	PlayerInputComponent->BindAction("DropCurrentWeapon", IE_Pressed, this, &ATDSCharacter::DropCurrentWeapon);
	
	PlayerInputComponent->BindAxis("MoveForward", this, &ATDSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATDSCharacter::InputAxisY);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	PlayerInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATDSCharacter::TKeyPressed<1>);
	PlayerInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATDSCharacter::TKeyPressed<2>);
	PlayerInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATDSCharacter::TKeyPressed<3>);
	PlayerInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATDSCharacter::TKeyPressed<4>);
	PlayerInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATDSCharacter::TKeyPressed<5>);
	PlayerInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATDSCharacter::TKeyPressed<6>);
	PlayerInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATDSCharacter::TKeyPressed<7>);
	PlayerInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATDSCharacter::TKeyPressed<8>);
	PlayerInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATDSCharacter::TKeyPressed<9>);
	PlayerInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATDSCharacter::TKeyPressed<0>);
}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::InputAttackPressed()
{
	if (!bIsAlive) return;
	AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDSCharacter::WalkingEnable()
{
	bWalking = true;
	ChangeMovementState();
}

void ATDSCharacter::WalkingDisable()
{
	bWalking = false;
	ChangeMovementState();
}

void ATDSCharacter::AimingEnable()
{
	bAiming = true;
	ChangeMovementState();

	/** BlueprintCallable for change camera distance when zoom */
	UpdateCameraDistance(MovementState);
}

void ATDSCharacter::AimingDisable()
{
	bAiming = false;
	ChangeMovementState();

	/** BlueprintCallable for change camera distance when zoom */
	UpdateCameraDistance(MovementState);
}

void ATDSCharacter::SprintingEnable()
{
	bSprinting = true;
	ChangeMovementState();
}

void ATDSCharacter::SprintingDisable()
{
	bSprinting = false;
	ChangeMovementState();
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	if (!CurrentWeapon) return;
	CurrentWeapon->SetWeaponStateFire_OnServer(bIsFiring);
}

void ATDSCharacter::TryAbilityAction()
{
	if (AbilityEffect)
	{
		UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

bool ATDSCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->GetWeaponReloading() && InventoryComponent->GetWeaponSlotsData().IsValidIndex(ToIndex))
	{
		if (CurrentWeaponIndex != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentWeaponIndex;
			FAdditionalWeaponData OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->GetWeaponReloading())
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
	return bIsSuccess;
}

void ATDSCharacter::DropCurrentWeapon()
{
	if (!InventoryComponent) return;
		
	FDropItemData DropItemData;
	InventoryComponent->DropWeaponByIndex(CurrentWeaponIndex, DropItemData);
}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::AIMING_STATE:
		ResSpeed = MovementInfo.AimingSpeed;
		break;
	case EMovementState::AIMING_WALK_STATE:
		ResSpeed = MovementInfo.AimingWalkSpeed;
		break;
	case EMovementState::WALK_STATE:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::RUN_STATE:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::SPRINT_STATE:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::RUN_STATE;

	if (!bSprinting && !bAiming && !bWalking)
	{
		NewState = EMovementState::RUN_STATE;
	}
	else
	{	
		if (bAiming)
		{
			bSprinting = false;
			bWalking = false;

			NewState = EMovementState::AIMING_STATE;
		}
		else
		{
			if (bSprinting && !bAiming)
			{
				NewState = EMovementState::SPRINT_STATE;
			}
			else
			{
				if (!bWalking)
				{
					NewState = EMovementState::RUN_STATE;
				}
				else
				{
					NewState = EMovementState::AIMING_WALK_STATE;
				}
			}
		}
	}

	SetMovementState_OnServer(NewState);
	//CharacterUpdate();

	AWeaponBase* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

void ATDSCharacter::DisplacementUpdate(const FHitResult& HitResult)
{
	if (!CurrentWeapon) return;
	
	FVector Displacement = FVector::ZeroVector;
	bool bIsReduceDispersion = false;

	switch (MovementState)
	{
	case EMovementState::AIMING_STATE:
		Displacement = FVector(0.0f, 0.0f, 160.0f);
		bIsReduceDispersion = true;
		//CurrentWeapon->bShouldReduceDispersion = true;
		break;
	case EMovementState::AIMING_WALK_STATE:
		Displacement = FVector(0.0f, 0.0f, 160.0f);
		bIsReduceDispersion = true;
		//CurrentWeapon->bShouldReduceDispersion = true;
		break;
	case EMovementState::WALK_STATE:
		Displacement = FVector(0.0f, 0.0f, 120.0f);
		//CurrentWeapon->bShouldReduceDispersion = false;
		break;
	case EMovementState::RUN_STATE:
		Displacement = FVector(0.0f, 0.0f, 120.0f);
		//CurrentWeapon->bShouldReduceDispersion = false;
		break;
	case EMovementState::SPRINT_STATE:
		break;
	default:
		break;
	}

	//CurrentWeapon->ShootEndLocation = HitResult.Location + Displacement;
	CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(HitResult.Location + Displacement, bIsReduceDispersion);
}

void ATDSCharacter::InitWeapon(FName WeaponNameID, FAdditionalWeaponData WeaponAdditionalInfo, int32 NewWeaponIndex)
{
	//On Server
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDSGameInstance* MyGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponData MyWeaponInfo;
	if (MyGI)
	{
		if (MyGI->GetWeaponInfoByName(WeaponNameID, MyWeaponInfo))
		{
			if (MyWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector::ZeroVector;;
				FRotator SpawnRotation = FRotator::ZeroRotator;

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponBase* MyWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(MyWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (MyWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = MyWeapon;

					MyWeapon->WeaponID_Name = WeaponNameID;
					MyWeapon->WeaponSettings = MyWeaponInfo;
					
					MyWeapon->ReloadTime = MyWeaponInfo.ReloadTime;
					MyWeapon->UpdateStateWeapon_OnServer(MovementState);		

					MyWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					CurrentWeaponIndex = NewWeaponIndex;

					// Not Forget remove delegate on change/drop weapon
					MyWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
					MyWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);

					MyWeapon->OnWeaponFireStart.AddDynamic(this, &ATDSCharacter::WeaponFireStart);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(MyWeapon->WeaponSettings.WeaponType);
				}
			}
		}
		else
		{
			UE_LOG(LogTDSCharacter, Warning, TEXT("Class ATDSCharacter::InitWeapon -> Weapon not found in table"));
		}
	}
}

/** Start weapon reload */
void ATDSCharacter::TryReloadWeapon()
{
	if (!bIsAlive) return;

	if (CurrentWeapon && !CurrentWeapon->GetWeaponReloading())
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSettings.ClipSize && CurrentWeapon->CheckCanWeaponReload())
			CurrentWeapon->InitReload();
	}
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSettings.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

void ATDSCharacter::WeaponFireStart(UAnimMontage* Anim) 
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->AdditionalWeaponInfo);

	WeaponFireStart_BP(Anim);
}

//ToDO in one func TrySwitchPreviosWeapon && TrySwicthNextWeapon
//need Timer to Switch with Anim, this method stupid i must know switch success for second logic inventory
//now we not have not success switch/ if 1 weapon switch to self
void ATDSCharacter::TrySwicthNextWeapon()
{
	if (!CurrentWeapon || CurrentWeapon->GetWeaponReloading() || !InventoryComponent->GetWeaponSlotsData().IsValidIndex(0)) return;
	SwitchWeapon(true);
}

void ATDSCharacter::TrySwitchPreviousWeapon()
{
	if (!CurrentWeapon || CurrentWeapon->GetWeaponReloading() || !InventoryComponent->GetWeaponSlotsData().IsValidIndex(0)) return;
	SwitchWeapon(false);
}

void ATDSCharacter::SwitchWeapon(bool bIsForward)
{
	int8 OldIndex = CurrentWeaponIndex;
	FAdditionalWeaponData OldAdditionalInfo;

	if (CurrentWeapon)
	{
		OldAdditionalInfo = CurrentWeapon->AdditionalWeaponInfo;
		if (CurrentWeapon->GetWeaponReloading())
			CurrentWeapon->CancelReload();
	}

	if (InventoryComponent)
	{
		int8 Index = 0;
		if (bIsForward)
			Index = CurrentWeaponIndex + 1;
		else
			Index = CurrentWeaponIndex - 1;

		if (InventoryComponent->SwitchWeaponToIndex(Index, OldIndex, OldAdditionalInfo, bIsForward))
		{}
	}
}

//** Get & Set functions */
class UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

AWeaponBase* ATDSCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

APlayerController* ATDSCharacter::GetPlayerController() const
{
	const auto PlayerController = Cast<APlayerController>(GetController());	
	return PlayerController ? PlayerController : nullptr;
}

/** Returns Movements mode */
bool ATDSCharacter::GetAimingMode() const {	return bAiming; }
bool ATDSCharacter::GetWalkingMode() const { return bWalking; }
bool ATDSCharacter::GetSprintingMode() const { return bSprinting; }


float ATDSCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	const float Result = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	
	if (bIsAlive && HealthComponent)
	{
		HealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if(DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		const auto Projectile = Cast<AProjectile>(DamageCauser);
		if (Projectile)
		{
			UTDSTypes::AddEffectBySurfaceType(this, NAME_None, Projectile->GetProjectileData().Effect, GetSurfaceType());
		}
	}

	return Result;
}

void ATDSCharacter::CharacterDead_BP_Implementation()
{}

void ATDSCharacter::CharacterDead()
{
	bIsAlive = false;

	float TimeAnimation = 0.0f;
	const int32 rnd = FMath::RandHelper(DeathsAnimation.Num());

	if (DeathsAnimation.IsValidIndex(rnd) && DeathsAnimation[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnimation = DeathsAnimation[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeathsAnimation[rnd]);
	}

	if (GetController())
	{
		GetController()->UnPossess();
	}

	UnPossessed();

	GetWorldTimerManager().SetTimer(RagdollTimer, this, &ATDSCharacter::EnableRagdoll, TimeAnimation, false);

	if (GetCursorToWorld())
		GetCursorToWorld()->SetVisibility(false);

	AttackCharEvent(false);
	CharacterDead_BP();
}

void ATDSCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

EPhysicalSurface ATDSCharacter::GetSurfaceType()
{
	if (!GetMesh() || !HealthComponent || HealthComponent->GetShieldValue() > 0) return EPhysicalSurface::SurfaceType_Default;

	const auto Material = GetMesh()->GetMaterial(0);
	return Material ? Material->GetPhysicalMaterial()->SurfaceType : EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDS_StateEffect*> ATDSCharacter::GetAllCurrentEffects() const
{
	return Effects;
}

void ATDSCharacter::RemoveEffect(UTDS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDSCharacter::AddEffect(UTDS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

int32 ATDSCharacter::GetCurrentWeaponIndex()
{
	return CurrentWeaponIndex;
}

bool ATDSCharacter::GetIsAlive() const
{
	return bIsAlive;
}

EMovementState ATDSCharacter::GetMovementState() const
{
	return MovementState;
}


void ATDSCharacter::SetActorRotationYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationYaw_Multicast(Yaw);
}

void ATDSCharacter::SetActorRotationYaw_Multicast_Implementation(float Yaw)
{
	if(Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATDSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ATDSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ATDSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDSCharacter, MovementState);
	DOREPLIFETIME(ATDSCharacter, CurrentWeapon);
}