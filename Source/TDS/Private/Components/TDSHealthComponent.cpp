#include "Components/TDSHealthComponent.h"

UTDSHealthComponent::UTDSHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UTDSHealthComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTDSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UTDSHealthComponent::GetCurrentHealth() const
{
	return Health;
}

void UTDSHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTDSHealthComponent::ChangeHealthValue(float Value)
{
	Value = Value * CoefDamage;
	Health += Value;

	OnHealthChange.Broadcast(Health, Value);

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health <= 0.0f)
		{
			OnDead.Broadcast();
		}
	}
}