#include "Components/TDSCharacterHealthComponent.h"

// Engine class include
#include "TimerManager.h"

DEFINE_LOG_CATEGORY_STATIC(LogCharacterHealthComponent, All, All)

void UTDSCharacterHealthComponent::ChangeHealthValue(float Value)
{
	float CurrentValue = Value * CoefDamage;
	
	if (Shield > 0.0f && Value < 0.0f)
	{
		ChangeShieldValue(Value);
		if (FMath::IsNearlyZero(Shield))
		{
			// Spawn FX
		}
	}
	else
	{
		Super::ChangeHealthValue(Value);
	}
}

void UTDSCharacterHealthComponent::ChangeShieldValue(float Value)
{
	Shield = FMath::Clamp(Shield + Value, 0.0f, 100.0f);
	OnShieldChange.Broadcast(Shield, Value);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(CoolDownShieldTimer, this, &UTDSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
	}
}

void UTDSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryRateTimer, this, &UTDSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTDSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}

float UTDSCharacterHealthComponent::GetShieldValue() const
{
	return Shield;
}