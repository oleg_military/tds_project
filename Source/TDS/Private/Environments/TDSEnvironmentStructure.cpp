#include "Environments/TDSEnvironmentStructure.h"

//Engine class include 
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

//Game class include
#include "StateEffects/TDS_StateEffect.h"

ATDSEnvironmentStructure::ATDSEnvironmentStructure()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATDSEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();	
}

EPhysicalSurface ATDSEnvironmentStructure::GetSurfaceType()
{
	const auto Mesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if(!Mesh) return EPhysicalSurface::SurfaceType_Default;

	const auto Material = Mesh->GetMaterial(0);	
	return Material ? Material->GetPhysicalMaterial()->SurfaceType : EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDS_StateEffect*> ATDSEnvironmentStructure::GetAllCurrentEffects() const
{
	return Effects;
}

void ATDSEnvironmentStructure::RemoveEffect(UTDS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDSEnvironmentStructure::AddEffect(UTDS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}