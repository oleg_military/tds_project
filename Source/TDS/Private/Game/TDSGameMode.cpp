#include "Game/TDSGameMode.h"

// UE4 class include
#include "UObject/ConstructorHelpers.h"

// My class include
#include "Player/TDSPlayerController.h"
#include "Player/TDSCharacter.h"

ATDSGameMode::ATDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDSPlayerController::StaticClass();
	
	// set default pawn class to our Blueprinted character
	/*static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TDS/Blueprints/Pawns/Player/BP_PlayerCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}*/
}

void ATDSGameMode::PlayerCharacterDead()
{}