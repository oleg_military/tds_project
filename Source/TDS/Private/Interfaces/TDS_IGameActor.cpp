#include "Interfaces/TDS_IGameActor.h"

//Game class include
#include "StateEffects/TDS_StateEffect.h"

EPhysicalSurface ITDS_IGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDS_StateEffect*> ITDS_IGameActor::GetAllCurrentEffects() const
{
	TArray<UTDS_StateEffect*> Effects;
	return Effects;
}

void ITDS_IGameActor::RemoveEffect(UTDS_StateEffect * RemoveEffect)
{}

void ITDS_IGameActor::AddEffect(UTDS_StateEffect* NewEffect)
{}