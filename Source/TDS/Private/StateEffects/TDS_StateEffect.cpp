#include "StateEffects/TDS_StateEffect.h"

//Engine class include
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

//Game class include
#include "Components/TDSHealthComponent.h"
#include "Interfaces/TDS_IGameActor.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDS_StateEffect, All, All);

bool UTDS_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	MyActor = Actor;

	ITDS_IGameActor* Interface = Cast<ITDS_IGameActor>(MyActor);
	if (Interface)
	{
		Interface->AddEffect(this);
	}

	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* Interface = Cast<ITDS_IGameActor>(MyActor);
	if (Interface)
	{
		Interface->RemoveEffect(this);
	}

	MyActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}	
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();	
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (MyActor)
	{
		auto HealthComponent = Cast<UTDSHealthComponent>(MyActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (HealthComponent)
		{
			HealthComponent->ChangeHealthValue(Power);
		}
	}	

	DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if(GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(ExicuteTimerHandle, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(EffectTimerHandle, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

		UE_LOG(LogTDS_StateEffect, Display, TEXT("UTDS_StateEffect_ExecuteTimer::StartTimer!"));

		if (ParticleEffect)
		{
			FName NameBoneToAttached = NameBoneHit;
			FVector Location = FVector::ZeroVector;

			const auto SK = Cast<USceneComponent>(MyActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
			if (SK)
			{
				ParticleComponent = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, SK, NameBoneToAttached, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
			else
			{
				ParticleComponent = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, MyActor->GetRootComponent(), NameBoneToAttached, Location, FRotator::ZeroRotator,
					EAttachLocation::SnapToTarget, false);
			}
		}
	}

	return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (ParticleComponent)
	{
		ParticleComponent->DestroyComponent();
		ParticleComponent = nullptr;
	}
	
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (MyActor)
	{
		auto HealthComponent = Cast<UTDSHealthComponent>(MyActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (HealthComponent)
		{
			HealthComponent->ChangeHealthValue(Power);
		}
	}
}