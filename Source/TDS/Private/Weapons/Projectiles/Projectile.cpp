#include "Weapons/Projectiles/Projectile.h"

//Engine class include
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Perception/AISense_Damage.h"
#include "Net/UnrealNetwork.h"

//Game class include
#include "StateEffects/TDS_StateEffect.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetReplicates(true);
	SetReplicateMovement(true);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	
	SphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);	
	SphereCollision->SetCollisionProfileName(TEXT("Projectile"));

	SphereCollision->SetSphereRadius(16.0f);

	/** Overlap Events */
	SphereCollision->OnComponentHit.AddDynamic(this, &AProjectile::OnProjectileHit);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnProjectileBeginOverlap);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &AProjectile::OnProjectileEndOverlap);

	SphereCollision->bReturnMaterialOnMove = true;
	SphereCollision->SetCanEverAffectNavigation(false);
	RootComponent = SphereCollision;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetCanEverAffectNavigation(false);
	BulletMesh->SetCollisionProfileName(TEXT("NoCollision"));
	BulletMesh->SetupAttachment(RootComponent);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	ProjectileMoveComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	ProjectileMoveComponent->UpdatedComponent = RootComponent;
	ProjectileMoveComponent->InitialSpeed = 0.0f;
	ProjectileMoveComponent->MaxSpeed = 1000.0f;
	ProjectileMoveComponent->bRotationFollowsVelocity = true;
	ProjectileMoveComponent->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();	
}

void AProjectile::InitProjectile(FFireData InitParams)
{
	ProjectileMoveComponent->InitialSpeed = InitParams.ProjectileData.ProjectileInitSpeed;
	ProjectileMoveComponent->MaxSpeed = InitParams.ProjectileData.ProjectileInitSpeed;
	this->SetLifeSpan(InitParams.ProjectileData.ProjectileLifeTime);

	if (BulletMesh && !BulletMesh->GetStaticMesh())
	{
		BulletMesh->DestroyComponent();
	}

	if (BulletFX && !BulletFX->Template)
	{
		BulletFX->DestroyComponent();
	}

	FireData = InitParams;
}

void AProjectile::OnProjectileHit(class UPrimitiveComponent* HitComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
		if (FireData.ImpactMapData.Contains(mySurfacetype))
		{
			const auto ImpactData = FireData.ImpactMapData[mySurfacetype];

			UMaterialInterface* myMaterial = ImpactData.DecalData.Material;
			if (myMaterial && OtherComp)
			{
				SpawnHitDecal_Multicast(myMaterial, ImpactData.DecalData.Size, OtherComp, Hit, ImpactData.DecalData.LifeTime);
			}
		
			if (ImpactData.HitEffect)
			{
				UParticleSystem* myParticle = ImpactData.HitEffect;
				if (myParticle)
				{
					SpawnHitFX_Multicast(myParticle, Hit);
				}
			}

			if (ImpactData.HitSound)
			{
				SpawnHitSound_Multicast(ImpactData.HitSound, Hit);
			}
		}

		UTDSTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, FireData.ProjectileData.Effect, mySurfacetype);
	}

	UGameplayStatics::ApplyPointDamage(OtherActor, FireData.ProjectileData.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), FireData.ProjectileData.ProjectileDamage, Hit.Location, Hit.Location);
	
	ImpactProjectile();
}

void AProjectile::OnProjectileBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{}

void AProjectile::OnProjectileEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{}

void AProjectile::ImpactProjectile()
{
	this->Destroy();
}

void AProjectile::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* Decalmaterial, FVector Size, UPrimitiveComponent* OtherComp, FHitResult Hit, float LifeTime)
{
	UGameplayStatics::SpawnDecalAttached(Decalmaterial, Size, OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, LifeTime);
}

void AProjectile::SpawnHitFX_Multicast_Implementation(UParticleSystem* TemplateFX, FHitResult Hit)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TemplateFX, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
}

void AProjectile::SpawnHitSound_Multicast_Implementation(USoundCue* Sound, FHitResult Hit)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Hit.ImpactPoint);
}