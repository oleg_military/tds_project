#include "Weapons/Projectiles/Projectile_Grenade.h"

// UE4 class include
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(TEXT("TDS.DebugExplode"), DebugExplodeShow, TEXT("Draw debug for Explode"), ECVF_Cheat);

DEFINE_LOG_CATEGORY_STATIC(LogProjectileBase_Grenade, All, All);

void AProjectile_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectile_Grenade::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	Explosion();
}

void AProjectile_Grenade::OnProjectileHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	Super::OnProjectileHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);

	if (FireData.ProjectileData.ExplosionData.ExplodeOnImpact)
	{
		Explosion();
	}
}

void AProjectile_Grenade::Explosion()
{
	const auto ExplosionData = FireData.ProjectileData.ExplosionData;

	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionData.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionData.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}

	TimerEnabled = false;
	if (ExplosionData.ExplosionFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionData.ExplosionFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}

	if (ExplosionData.ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionData.ExplosionSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ExplosionData.ExplodeMaxDamage, ExplosionData.ExplodeMaxDamage * ExplosionData.ExplodeFalloffCoef,
		GetActorLocation(), ExplosionData.ProjectileMinRadiusDamage, ExplosionData.ProjectileMaxRadiusDamage, 5,
		NULL, IgnoredActor, this, nullptr);

	this->Destroy();
}

void AProjectile_Grenade::ImpactProjectile()
{}