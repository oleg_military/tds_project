#include "Weapons/Items/DropShell.h"

// UE4 class include
#include "GameFramework/RotatingMovementComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogDropShell, All, All);

// Sets default values
ADropShell::ADropShell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	DropStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DropStaticMesh"));	
	DropStaticMesh->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
	DropStaticMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	
	DropStaticMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	DropStaticMesh->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	DropStaticMesh->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	DropStaticMesh->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Block);	
	
	DropStaticMesh->SetNotifyRigidBodyCollision(true);
	DropStaticMesh->SetSimulatePhysics(true);	
	RootComponent = DropStaticMesh;

	DropStaticMesh->OnComponentHit.AddDynamic(this, &ADropShell::OnHitComponent);

	RotMovemetComponent = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotMovemetComponent"));
	RotMovemetComponent->RotationRate = FRotator(40.0f, 180.0f, 40.0f);

	MovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComp"));
	MovementComp->InitialSpeed = 150.0f;
}

void ADropShell::OnHitComponent(class UPrimitiveComponent* HitComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, 
	FVector NormalImpulse, const FHitResult& Hit)
{
	if (bRotationComponent)
	{
		bRotationComponent = false;
		RotMovemetComponent->DestroyComponent(true);
	}	
}

void ADropShell::SetDropImpulse(float Value)
{
	MovementComp->InitialSpeed = Value;
}